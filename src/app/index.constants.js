/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('cplanding')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
