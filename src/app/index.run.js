(function() {
  'use strict';

  angular
    .module('cplanding')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
